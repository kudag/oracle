package rooms;

import rooms.enums.RoomType;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Room  implements Serializable {
    private Set<Room> neighbourRooms;
    private RoomType roomType;
    private String name;

    public Room(Set<Room> neighbourRooms, RoomType roomType, String name){
        this.neighbourRooms = neighbourRooms;
        this.roomType = roomType;
        this.name = name;
    }
    public Room(RoomType roomType, String name){
        this.neighbourRooms = new HashSet<>();
        this.roomType = roomType;
        this.name = name;
    }

    public Room(){this.neighbourRooms = new HashSet<>();}

    public RoomType getRoomType() {
        return roomType;
    }
    public void setRoomType(RoomType roomType){
        this.roomType = roomType;
    }

    public Set<Room> getNeighbourRooms() {
        return neighbourRooms;
    }
    public void setNeighbourRooms(Set<Room> neighbourRooms){
        this.neighbourRooms = neighbourRooms;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Room addNeighbourRoom(Room room){
        if(!neighbourRooms.contains(room)){
            neighbourRooms.add(room);
        }

        return this;
    }
}
