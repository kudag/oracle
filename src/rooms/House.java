package rooms;

import rooms.enums.RoomType;

import java.io.Serializable;
import java.util.*;

public class House  implements Serializable {
    private Room entrance;
    private Set<Room> rooms;

    public House(){this.rooms = new HashSet<>();}

    public House(Room entrance){
        this.entrance = entrance;
        this.rooms = new HashSet<>();
    }

    public House(Room entrance, Set<Room> rooms){
        this.entrance = entrance;
        this.rooms = rooms;
    }

    public Room getEntrance() {
        return entrance;
    }
    public void setEntrance(Room entrance){
        this.entrance = entrance;
    }

    public Set<Room> getRooms() {
        return rooms;
    }
    public void setRooms(Set<Room> rooms){
        this.rooms = rooms;
    }

    //add rooms
    public void addRoom(Room room){
        if(!rooms.contains(room)){
            rooms.add(room);
        }
    }

    //convert to binary

    //print rooms


    //print path from entrance to exit
    public List<Room> getExitPath(){
        Set<Room> visitedRooms = new HashSet<Room>();

        //list to contain final result
        List<Room> finalRoomNames = new ArrayList<>();

        //perform dfs from entrance
        dfs(entrance, finalRoomNames, visitedRooms);

        //reverse list
        Collections.reverse(finalRoomNames);

        return finalRoomNames;
    }

    private List<Room> dfs(Room currentRoom, List<Room> finalRoomNames, Set<Room> visitedRooms){

        visitedRooms.add(currentRoom);
        //System.out.print(currentRoom.getName()+"<->");
        //check if exit room
        if(currentRoom.getRoomType() == RoomType.EXIT){
            finalRoomNames.add(currentRoom);
        }

        //check if room does not have any neighbours
        else if(currentRoom.getNeighbourRooms().size() != 0){
            //add all neighbours of current room into stack to visit
            for (Room room: currentRoom.getNeighbourRooms()) {
                if(!visitedRooms.contains(room)){

                    //visit neighbours
                    if(dfs(room, finalRoomNames, visitedRooms).size() > 0){
                        finalRoomNames.add(currentRoom);
                        break;
                    }
                }
            }

        }

        return finalRoomNames;
    }
}
