package rooms.enums;

public enum RoomType {
    ENTRANCE(0),
    EXIT(0),
    EMPTY(0),
    MONSTER(-1),
    TREASURE(1);

    private final int points;
    RoomType(int points) { this.points = points; }
    public int getValue() { return points; }
}
