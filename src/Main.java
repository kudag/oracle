import rooms.House;
import utils.Play;
import utils.Utils;

public class Main {

    public static void main(String[] args) {
        //create a house
        House house = Utils.build();

        /*print house and path to exit*/
        //Utils.printHouse(house);
        //Utils.printRoomList(house.getExitPath(), true);

        //test serialization and deserialization to bytes
        /*try{
            byte[] houseToBytes = Utils.serializeToBytes(house);
            System.out.print(houseToBytes);
            House deserializedHouse = (House)Utils.deserializeFromBytes(houseToBytes);

            //print deserialized house and path to exit
            Utils.printHouse(deserializedHouse);
            Utils.printRoomList(deserializedHouse.getExitPath(), true);
        }catch (Exception ex){
            System.out.println(ex);
        }
        */
        
        Play.playGame(house);
    }
}
