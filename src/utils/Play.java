package utils;


import rooms.House;
import rooms.Room;
import rooms.enums.RoomType;
import utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Play {
    public static void playGame(House house) {
        List<Room> visitedRooms = new ArrayList<>();

        int input = 0;
        int score = 0;
        Room currentRoom = house.getEntrance();

        System.out.println("You are on the entrance room. Find the way to the Exit.\n");

        do {
            Scanner scanner = new Scanner(System.in);

            System.out.println("Please enter your option to open a room.\n");
            visitedRooms.add(currentRoom);

            int roomNumber = 0;
            List<Room> neighboursList = new ArrayList<>(currentRoom.getNeighbourRooms());

            for (Room room : neighboursList) {
                System.out.println(roomNumber++ + ": " + room.getName());
            }

            System.out.println(-1 + ": I give up.\n");

            if (scanner.hasNextInt()) {
                input = scanner.nextInt();

                if (!(input <= -1 || input >=  neighboursList.size())) {
                    currentRoom = neighboursList.get(input);
                    if (currentRoom.getRoomType() == RoomType.EXIT) {
                        System.out.println("Congratulations you have finaly found the exit room!");
                        visitedRooms.add(currentRoom);
                        break;
                    } else {
                        if (currentRoom.getRoomType() == RoomType.MONSTER)
                            System.out.println("You are in the monster room \\-[-(0)-]-/");
                        else if (currentRoom.getRoomType() == RoomType.TREASURE)
                            System.out.println("You are in the treasure room $$$:-)$$$");
                        else
                            System.out.println("There is nothing in this room");
                    }
                    score += currentRoom.getRoomType().getValue();
                    System.out.println("Your current score is: " + score);

                    System.out.println("Press any key to continue...");
                    Scanner keyboard = new Scanner(System.in);
                    keyboard.nextLine();
                    System.out.println("\n======================================\n");
                }else {
                    System.out.println("!!!Please enter valid numbers to select a room.!!!\n");
                }

            } else {
                System.out.println("!!!Please enter numbers as indicated, try again.!!!\n");
            }

        } while (currentRoom.getRoomType() != RoomType.EXIT && input != -1);

        //Print Game Summary
        System.out.println("\n======================GAME OVER======================\n");
        System.out.println("Game Summary\n");
        System.out.println("\nPath you walked from room to room");
        Utils.printRoomList(visitedRooms, true);
        System.out.println("\nThe quicket path to exit was supposed to be");
        Utils.printRoomList(house.getExitPath(), true);
        System.out.println("\nThank you for playing");
    }
}
