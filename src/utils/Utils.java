package utils;

import rooms.House;
import rooms.Room;
import rooms.enums.RoomType;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    //print a given list of rooms
    public static void printRoomList(List<Room> rooms, boolean showScore){
        int pathScore = 0;

        for(int i = 0; i < rooms.size(); i++){
            pathScore += rooms.get(i).getRoomType().getValue();
            if(i != rooms.size() - 1)
                System.out.print(rooms.get(i).getName()+"->");
            else
                System.out.print(rooms.get(i).getName());
        }
        if(showScore)
            System.out.println("\nScore for the path is " + pathScore);
    }

    //print all rooms on a house and their next door
    public static void printHouse(House house){
        for (Room room: house.getRooms()){
            System.out.print((room.getRoomType() == RoomType.ENTRANCE? "(Entrance)":
                    room.getRoomType() == RoomType.EXIT? "(Exit)" : "") + room.getName() + "<->");
            printRoomList(new ArrayList<>(room.getNeighbourRooms()), false);
            System.out.println();
        }
        System.out.println("\n");
    }

    //serialize object to bytes
    public static byte[] serializeToBytes(Object obj) throws IOException {
        byte[] bytes = null;
        ByteArrayOutputStream bos = null;
        ObjectOutputStream oos = null;
        try {
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(obj);
            oos.flush();
            bytes = bos.toByteArray();
        } finally {
            if (oos != null) {
                oos.close();
            }
            if (bos != null) {
                bos.close();
            }
        }

        return bytes;
    }

    //deserialize bytes to obj
    public static Object deserializeFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        Object obj = null;
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;
        try {
            bis = new ByteArrayInputStream(bytes);
            ois = new ObjectInputStream(bis);
            obj = ois.readObject();
        } finally {
            if (bis != null) {
                bis.close();
            }
            if (ois != null) {
                ois.close();
            }
        }
        return obj;
    }

    //this method will build a house similar to the one given in our example, starting from the left room going to the right
    //if given an input of houses as an adjacent list, this will be easy to automate
    public static House build(){
        //left most room
        Room room1 = new Room(RoomType.TREASURE, "Room1");
        Room room2 = new Room(RoomType.MONSTER, "Room2");
        Room room3 = new Room(RoomType.MONSTER, "Room3");

        //rooms down
        Room room4 = new Room(RoomType.EMPTY, "Room4");
        Room room5 = new Room(RoomType.TREASURE, "Room5");

        //back to top
        Room room6 = new Room(RoomType.ENTRANCE, "Room6");
        Room room7 = new Room(RoomType.EMPTY, "Room7");

        //down
        Room room8 = new Room(RoomType.MONSTER, "Room8");
        Room room9 = new Room(RoomType.EMPTY, "Room9");
        Room room10 = new Room(RoomType.MONSTER, "Room10");

        //right from room9
        Room room11 = new Room(RoomType.TREASURE, "Room11");
        Room room12 = new Room(RoomType.EXIT, "Room12");//exit

        //back to top, last 3 room
        Room room13 = new Room(RoomType.MONSTER, "Room13");
        Room room14 = new Room(RoomType.TREASURE, "Room14");
        Room room15 = new Room(RoomType.MONSTER, "Room15");



        House house = new House(room6);

        //create an adjacent list of each room and add into the house
        house.addRoom(room1.addNeighbourRoom(room2));
        house.addRoom(room2.addNeighbourRoom(room1).addNeighbourRoom(room3));
        house.addRoom(room3.addNeighbourRoom(room2).addNeighbourRoom(room4).addNeighbourRoom(room6));
        house.addRoom(room4.addNeighbourRoom(room3).addNeighbourRoom(room5));
        house.addRoom(room5.addNeighbourRoom(room4));
        house.addRoom(room6.addNeighbourRoom(room3).addNeighbourRoom(room7));
        house.addRoom(room7.addNeighbourRoom(room6).addNeighbourRoom(room8).addNeighbourRoom(room13));
        house.addRoom(room8.addNeighbourRoom(room7).addNeighbourRoom(room9));
        house.addRoom(room9.addNeighbourRoom(room8).addNeighbourRoom(room10).addNeighbourRoom(room11));
        house.addRoom(room10.addNeighbourRoom(room9));
        house.addRoom(room11.addNeighbourRoom(room9).addNeighbourRoom(room12));
        house.addRoom(room12.addNeighbourRoom(room11));
        house.addRoom(room13.addNeighbourRoom(room7).addNeighbourRoom(room14).addNeighbourRoom(room15));
        house.addRoom(room14.addNeighbourRoom(room13));
        house.addRoom(room15.addNeighbourRoom(room13));

        return house;
    }


}
